package com.example.amir.healthhome2.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import java.util.HashMap;

public class Pref {


    public static final String KEY_SOUND = "setting_sound";
    public static final String KEY_NOTIFO = "setting_notifo";
	public static final String KEY_FIRST_TIME  = "firsttime";
	public static final String PREF_NAME = "hah";
	public static final String KEY_ID = "id";
	private static final String IS_LOGIN = "IsLoggedIn";
	public static final String KEY_NAME = "name";
	public static final String KEY_PASSWORD = "password";
	public static final String KEY_TOKEN="remember_token";

	public static final String P_CODE = "code";
	public static final String P_NAME = "patient_name";
	public static final String NURSE_NAME = "nurse_name";
	public static final String P_ID = "p_id";
	public static final String SELF_CARE = "self_care";
	public static final String SPHINCTER = "sphincter";
	public static final String MOBILITY = "mobility";
	public static final String COMMUNICATION = "communication";
	public static final String ADL_MSG = "adl_msg";

	public static final String nurseClicked = "";
	public static final String patientClicked = "" ;



    Context ctx;
	SharedPreferences preferences;

	public Pref(Context ctx){
		this.ctx = ctx;
		preferences = PreferenceManager.getDefaultSharedPreferences(ctx);
		initializeDefaults();
	}

	public void initializeDefaults(){
        if(!containsKey(KEY_NOTIFO)){
            setBoolPreferences(KEY_NOTIFO, true);
        }
        if(!containsKey(KEY_SOUND)){
            setBoolPreferences(KEY_SOUND, true);
        }
        if(!containsKey(KEY_FIRST_TIME)){
            setBoolPreferences(KEY_FIRST_TIME, true);
        }
	}
	
	public boolean containsKey(String key){
		return preferences.contains(key);
	}
	
	public String getPreferences(String key){
		return preferences.getString(key, "");
	}
	
	public boolean setPreferences(String key, String value){
		return preferences.edit().putString(key, value).commit();
	}
	public int getIntPreferences(String key){
		return preferences.getInt(key, 0);
	}
	
	public boolean setIntPreferences(String key, int value){
		return preferences.edit().putInt(key, value).commit();
	}
	
	public long getLongPreferences(String key){
		return preferences.getLong(key, 0l);
	}
	
	public boolean setLongPreferences(String key, long value){
		return preferences.edit().putLong(key, value).commit();
	}
	
	public boolean getBoolPreferences(String key){
		return preferences.getBoolean(key, false);
	}
	public boolean getBoolPreferences(String key, boolean fallback){
		return preferences.getBoolean(key, fallback);
	}
	
	public boolean setBoolPreferences(String key, boolean value){
		return preferences.edit().putBoolean(key, value).commit();
	}
	
	public boolean clearPreferences(){
		return preferences.edit().clear().commit();



	}
	public boolean clearPatientID(){
		return preferences.edit().remove(P_ID).commit();
	}
	public boolean clearADL(){
	return preferences.edit().remove(SELF_CARE).remove(SPHINCTER).remove(COMMUNICATION).remove(MOBILITY).remove(ADL_MSG).commit();

	}
	public boolean clearCounter(){
		return preferences.edit().remove(nurseClicked).remove(patientClicked).commit();
	}

	public boolean isLoggedIn(){
		return !preferences.getString("u_token", "").isEmpty();
	}

	public void logoutUser(){
		clearPreferences();
	}
	public void logoutPatient(){
		clearPatientID();
	}
	public void clearAdl(){
		clearADL();
	}
	public void clearcounter(){
		clearCounter();
	}

	/**
	 * Get stored session data
	 * */
	public HashMap<String, String> getUserDetails(){
		HashMap<String, String> user = new HashMap<String, String>();
		user.put(KEY_NAME, preferences.getString(KEY_NAME, null));
		user.put(KEY_PASSWORD, preferences.getString(KEY_PASSWORD, null));
		user.put(KEY_TOKEN, preferences.getString(KEY_TOKEN, null));

		user.put(P_ID, preferences.getString(P_ID, null));
		user.put(P_CODE, preferences.getString(P_CODE, null));

		return user;
	}

	public void createLoginSession(String name, String password, String role){
		// Storing activity_login value as TRUE
		setBoolPreferences(IS_LOGIN, true);

		// Storing name in pref
		setPreferences(KEY_NAME, name);

		// Storing email in pref
		setPreferences(KEY_PASSWORD, password);

		//editor.putString(KEY_TOKEN,remember_token);
	}

	public void saveId(String id) {
		setPreferences(KEY_ID,id);
	}
	public String getId(){
		return getPreferences(KEY_ID);
	}

	public void createToken(String token) {
		setPreferences(KEY_TOKEN,token);
	}
}
