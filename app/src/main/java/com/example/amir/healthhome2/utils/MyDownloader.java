package com.example.amir.healthhome2.utils;

import android.app.AlertDialog;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.v4.app.NotificationCompat;
import android.widget.Toast;

import com.example.amir.healthhome2.Main;
import com.example.amir.healthhome2.R;
import com.example.amir.healthhome2.utilities.Utilities;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;

public class MyDownloader extends AsyncTask<Void, Integer, String> {
	String link;
	Context context;
	Boolean running = true;
	int index;
	File file;
    int id = 1;
	onDownloadCompleteListner action;

	public MyDownloader(Context context, String link) {
		this.link = link;
		this.context = context;
	}

	public MyDownloader(Context context, int index, String link,
						onDownloadCompleteListner action) {
		this.link = link;
		this.context = context;
		this.index = index;
		this.action = action;
	}

	public static interface onDownloadCompleteListner {
		public void downloadListner(int index, String link, boolean completed);
	}

	@Override
	protected String doInBackground(Void... params) {
		int count;
		try {
			URL links = new URL(link);
			URLConnection conection = links.openConnection();
			conection.connect();
			int lenghtOfFile = conection.getContentLength();
			InputStream input = new BufferedInputStream(links.openStream(),
					8 * 1024);
			file = Utilities.getSDPathFromUrl(link);
			OutputStream output = new FileOutputStream(file);
			byte data[] = new byte[8 * 1024];
			int total = 0;
			int lastProgress = 0;
			if (running) {
				while ((count = input.read(data)) != -1) {
					total += count;
					output.write(data, 0, count);
					int progress = ((total * 100) / lenghtOfFile);
					if (lastProgress < progress) {
						lastProgress = progress;
						publishProgress(progress);
					}
				}
			}
			output.flush();
			output.close();
			input.close();
			return file.getPath();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	protected void onPreExecute() {
		super.onPreExecute();
		initializeProgressBar();
		initializeProgressBox();
		mBuilder.setProgress(100, 0, false);
		notificationManager.notify(id, mBuilder.build());

	}

	@Override
	protected void onPostExecute(String result) {
		super.onPostExecute(result);
		if (!running && file.exists()) {
			file.delete();
		}
		mBuilder.setContentText("Download complete").setProgress(0, 0, false);
        if(new Pref(context).getBoolPreferences(Pref.KEY_SOUND)) {
            mBuilder.setDefaults(Notification.DEFAULT_ALL);
        }
		notificationManager.notify(id, mBuilder.build());
		prgbar.dismiss();

		showAlert();

		if (action != null) {
			action.downloadListner(index, link, running);
		}

	}

	private void showAlert() {
		if (context == null) {
			return;
		}
		AlertDialog.Builder builder = new AlertDialog.Builder(context);
		builder.setTitle("Download Completed")
				.setMessage("Do you want to open now ?")
				.setPositiveButton("Read Now",
						new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog,
									int which) {
								readPdfFile(context, file);
							}

						}).setNegativeButton("Read Later", null).create()
				.show();

	}

	public static void readPdfFile(Context context, File file) {
		if (file == null) {
			return;
		}
		try {
			Intent i = new Intent(Intent.ACTION_VIEW);
			i.setDataAndType(Uri.fromFile(file), "application/pdf");
			context.startActivity(i);

		} catch (ActivityNotFoundException e) {
			Toast.makeText(context,
                    "No application available to fiew this type of file. :(",
                    Toast.LENGTH_LONG).show();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}


	NotificationCompat.Builder mBuilder;
	NotificationManager notificationManager;
	Uri sound;

	private void initializeProgressBar() {

		mBuilder = new NotificationCompat.Builder(context);
		mBuilder.setContentTitle(context.getString(R.string.app_name));
		mBuilder.setSmallIcon(R.mipmap.ic_launcher);
		notificationManager = (NotificationManager) context
				.getSystemService(Context.NOTIFICATION_SERVICE);
		sound = RingtoneManager
				.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
		if (sound == null) {
			sound = RingtoneManager
					.getDefaultUri(RingtoneManager.TYPE_RINGTONE);
		}
		Intent intent = new Intent(context, Main.class)
				.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
						| Intent.FLAG_ACTIVITY_NEW_TASK);
		PendingIntent pendingIntent = PendingIntent.getActivity(context, 0,
                intent, PendingIntent.FLAG_UPDATE_CURRENT);
		mBuilder.setContentIntent(pendingIntent);
	}

	ProgressDialog prgbar;

	private void initializeProgressBox() {

		prgbar = new ProgressDialog(context);
		prgbar.setIcon(R.mipmap.ic_launcher);
		prgbar.setTitle("Downloading ...");
		prgbar.setMessage("Please wait");
		prgbar.setMax(100);
		prgbar.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
		prgbar.setButton(DialogInterface.BUTTON_POSITIVE, "Hide",
				new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						prgbar.dismiss();

					}
				});
		prgbar.setButton(DialogInterface.BUTTON_NEGATIVE, "Cancel",
				new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {

						if (Status.RUNNING != null) {
							cancel(true);
							running = false;
						}
						mBuilder.setContentText(" Oops!!! Download cancelled")
								.setProgress(0, 0, false);
						notificationManager.notify(id, mBuilder.build());
					}
				});
		prgbar.show();
	}

	@Override
	protected void onProgressUpdate(Integer... values) {
		mBuilder.setProgress(100, values[0], false);
		mBuilder.setContentText(values[0] + "% complete please wait...");
		prgbar.setProgress(values[0]);
		notificationManager.notify(id, mBuilder.build());
		super.onProgressUpdate(values);
	}

}
