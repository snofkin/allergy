package com.example.amir.healthhome2;

import android.app.Dialog;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.provider.Settings;
import android.speech.RecognizerIntent;
import android.support.annotation.Nullable;

import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TabHost;
import android.widget.TextView;
import android.widget.Toast;

import com.example.amir.healthhome2.http.MyDataQuery;
import com.example.amir.healthhome2.http.OnDataReceived;
import com.example.amir.healthhome2.utilities.Utilities;

import org.json.JSONException;
import org.json.JSONObject;

import br.com.mauker.materialsearchview.MaterialSearchView;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

import static android.R.attr.data;

/**
 * Created by Amir on 5/24/2017.
 */

public class Main extends AppCompatActivity implements OnDataReceived {
    String[] items;
    ArrayList<String> Listitems;
    ArrayAdapter<String> adapter;
    AutoCompleteTextView Food_Search, Environment_Search, Drug_Search, Reaction_Search;
    RadioGroup Onset_RadioGroup, Servity_RadioGroup, Status_Group;
    RadioButton Active, InActive,
            Mild_Radio, Mid_Radio, Moderate_Radio, Server_Radio,
            Childhood_Radio, Adulthood_Radio, Unknown_Radio;
    EditText Notes, Date;
    Button save;
    MyDataQuery db;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        db = new MyDataQuery(Main.this,this);

        Notes = (EditText) findViewById(R.id.notes);
        Date = (EditText) findViewById(R.id.date_view);
        Food_Search = (AutoCompleteTextView) findViewById(R.id.food_search);
        Drug_Search = (AutoCompleteTextView) findViewById(R.id.drug_search);
        Environment_Search = (AutoCompleteTextView) findViewById(R.id.environment_search);
        Reaction_Search = (AutoCompleteTextView) findViewById(R.id.reaction);

        Onset_RadioGroup = (RadioGroup) findViewById(R.id.onset_radiogroup);
        Servity_RadioGroup = (RadioGroup) findViewById(R.id.servity_radiogroup);
        Status_Group = (RadioGroup) findViewById(R.id.status_radiogroup);

        Active = (RadioButton) findViewById(R.id.active);
        InActive = (RadioButton) findViewById(R.id.inactive);
        Mild_Radio = (RadioButton) findViewById(R.id.mild_radio);
        Mid_Radio = (RadioButton) findViewById(R.id.mid_radio);
        Moderate_Radio = (RadioButton) findViewById(R.id.moderate_radio);
        Server_Radio = (RadioButton) findViewById(R.id.server_radio);
        Childhood_Radio = (RadioButton) findViewById(R.id.childhood_radio);
        Adulthood_Radio = (RadioButton) findViewById(R.id.adulthood_radio);
        Unknown_Radio = (RadioButton) findViewById(R.id.unknown_radio);

        save = (Button) findViewById(R.id.save_button);

        TabHost tab_host = (TabHost) findViewById(R.id.tab_host);
        tab_host.setup();


        TabHost.TabSpec tab1 = tab_host.newTabSpec("Tab 1");
        tab1.setContent(R.id.tab1);
        tab1.setIndicator("Drugs");

        tab_host.addTab(tab1);
        AutoCompleteTextView textView = (AutoCompleteTextView) findViewById(R.id.drug_search);
        textView.setTextSize(18);

        String[] drugs = getResources().getStringArray(R.array.Drugs);
        ArrayAdapter<String> adapter =
                new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, drugs);
        textView.setAdapter(adapter);

        TabHost.TabSpec tab2 = tab_host.newTabSpec("Tab 2");
        tab2.setContent(R.id.tab2);
        tab2.setIndicator("Food");
        tab_host.addTab(tab2);
        textView = (AutoCompleteTextView) findViewById(R.id.food_search);
        textView.setTextSize(18);
        String[] food = getResources().getStringArray(R.array.food);
        adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, food);
        textView.setAdapter(adapter);

        TabHost.TabSpec tab3 = tab_host.newTabSpec("Tab 3");
        tab3.setContent(R.id.tab3);
        tab3.setIndicator("Environment");
        textView.setTextSize(18);
        tab_host.addTab(tab3);
        textView = (AutoCompleteTextView) findViewById(R.id.environment_search);
        String[] environment = getResources().getStringArray(R.array.environment);
        adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, environment);
        textView.setAdapter(adapter);


//        for reaction
         textView = (AutoCompleteTextView) findViewById(R.id.reaction);
        String[] reaction = getResources().getStringArray(R.array.reaction);
        adapter = new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,reaction);
        textView.setAdapter(adapter);

        //date picker
        EditText eyeExam = (EditText) findViewById(R.id.date_view);
        Food_Search.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                Food_Search.showDropDown();
                return false;
            }
        });

Drug_Search.setOnTouchListener(new View.OnTouchListener() {
    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {
        Drug_Search.showDropDown();
        return false;
    }
});
Environment_Search.setOnTouchListener(new View.OnTouchListener() {
    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {
        Environment_Search.showDropDown();
        return false;
    }
});
        Reaction_Search.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                Reaction_Search.showDropDown();
                return false;
            }
        });
        eyeExam.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    DateDialog dialoge = new DateDialog(v);
                    FragmentTransaction ft = getFragmentManager().beginTransaction();
                    dialoge.show(ft, "DatePicker");

                }
            }
        });
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String drug_search = Drug_Search.getText().toString();
                String note = Notes.getText().toString();
                String food_search = Food_Search.getText().toString();
                String environment_search = Environment_Search.getText().toString();
                String reaction_search = Reaction_Search.getText().toString();
                String date = Date.getText().toString();
                String onsetradiogroup = ((RadioButton) findViewById(Onset_RadioGroup.getCheckedRadioButtonId())).getText().toString();
                String servityradiogroup = ((RadioButton) findViewById(Servity_RadioGroup.getCheckedRadioButtonId())).getText().toString();
                String statusradiogroup = ((RadioButton) findViewById(Status_Group.getCheckedRadioButtonId())).getText().toString();

if (drug_search.isEmpty() && food_search.isEmpty() && environment_search.isEmpty() || reaction_search.isEmpty()) {
    Toast.makeText(Main.this,

            "please fill the details properly"


            , Toast.LENGTH_SHORT).show();
}else {
    HashMap<String, String> data = new HashMap<String, String>();
    if (Utilities.isNetworkAvialable(getApplicationContext())) {
        data.put("user_id", "1");
        data.put("drug", drug_search);
        data.put("food", food_search);
        data.put("environment", environment_search);
        data.put("severity", servityradiogroup);
        data.put("reaction", reaction_search);
        data.put("onset", onsetradiogroup);
        data.put("date", date);
        data.put("comment", note);
        data.put("status", statusradiogroup);
        db.getRequestData("http://52.41.165.18/api/allergies", data);
    } else {
        showErrorMessage();
    }
}
            }
        });
    }

    private void showErrorMessage() {
        final Dialog dialog = new Dialog(Main.this);
        dialog.setContentView(R.layout.nointernet);
        dialog.setTitle("Sorry");

        Button button = (Button) dialog.findViewById(R.id.dismiss);

        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                dialog.dismiss();
//							finish();
                startActivityForResult(new Intent(Settings.ACTION_WIFI_SETTINGS), 0);
            }
        });
        dialog.show();

    }


    @Override
    public void onSuccess(String table_name, String result) {
        try {
            JSONObject obj = new JSONObject(result);
            String success = obj.getString("status");
            Log.i("status", success);
            Toast.makeText(this, success, Toast.LENGTH_SHORT).show();


            if (success.equals("400")) {
                Toast.makeText(this, "Synced", Toast.LENGTH_SHORT).show();
            } else {


                Toast.makeText(this, "Syncing error !400", Toast.LENGTH_SHORT).show();
            }
        } catch (JSONException e) {
            Toast.makeText(this, "Syncing error catch", Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }

    }

    @Override
    public void onError(String table_name, int status, String message) {
        Toast.makeText(this, "Syncing error onerror", Toast.LENGTH_SHORT).show();
    }
}
