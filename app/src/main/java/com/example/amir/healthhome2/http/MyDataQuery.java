package com.example.amir.healthhome2.http;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;

import com.example.amir.healthhome2.utils.Pref;

import java.util.HashMap;


/**
 * Created by hereon 9/2/15.
 */
public class MyDataQuery {
    Context context;
    OnDataReceived action;
    Pref pf;

    public MyDataQuery(Context context, OnDataReceived action){
        this.context = context;
        this.action = action;
        pf = new Pref(context);
    }



//    public void getRequestData(String table){
//        getRequestData(table, 0);
//    }



//    public void getRequestData(String table, int skip){
//        getRequestData(table, getRequestParameters(table, skip));
//    }

    public void getRequestData(final String table, final HashMap<String, String> params){
        new AsyncTask<Void, Void, String>(){
         //   ProgressDialog progressDialog;
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
              //  progressDialog = ProgressDialog.show(context, "Processing", "Please wait...", false, false);
            }

            @Override
            protected String doInBackground(Void... voids) {
                return new ServerRequest(context).httpPostData(table,params);
            }

            @Override
            protected void onPostExecute(String result) {
                super.onPostExecute(result);
             //   progressDialog.dismiss();
                if(action != null){
                    action.onSuccess(table, result);
                }
            }
        }.execute();
    }

    public void getRequestDataUrl(final String table){
            new AsyncTask<Void, Void, String>() {
                ProgressDialog progressDialog;
                @Override
                protected void onPreExecute() {
                    super.onPreExecute();
                 //   progressDialog = ProgressDialog.show(context,"Processing","Please wait",false,false);
                }

                @Override
                protected String doInBackground(Void... voids) {
                    return new ServerRequest(context).httpGetData(table);

                }

                @Override
                protected void onPostExecute(String result) {
                    super.onPostExecute(result);
                //    progressDialog.dismiss();
                    if (action != null) {
                            action.onSuccess(table, result);
                    }
                }
            }.execute();
    }
//    public HashMap<String, String> getRequestParameters(String table, int skip) {
//        HashMap<String, String> params = new HashMap<String, String>();
//        params.put("action", table);
//        params.put("start", skip + "");
//        params.put("limit", "20");
//        if(table.startsWith("st_")){
//            params.put("course_id", pf.getPreferences("st_course_id"));
//            params.put("sem", pf.getPreferences("st_sem"));
//        }
//        return params;
//    }
}
