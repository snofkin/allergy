package com.example.amir.healthhome2.http;

/**
 * Created by hereshem on 4/9/15.
 */
public interface OnDataReceived {
    void onSuccess(String table_name, String result);
    void onError(String table_name, int status, String message);
};