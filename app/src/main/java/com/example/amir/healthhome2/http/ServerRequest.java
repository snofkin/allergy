package com.example.amir.healthhome2.http;

import android.content.Context;
import android.net.ConnectivityManager;
import android.util.Log;

import com.example.amir.healthhome2.utilities.Utilities;
import com.example.amir.healthhome2.utils.Constants;
import com.example.amir.healthhome2.utils.Pref;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.security.MessageDigest;
import java.util.HashMap;
import java.util.Map;


public class ServerRequest {

    Context context;
    String action;
    public ServerRequest(Context context){
        this.context = context;
    }

	public static boolean isNetworkConnected(Context ctx) {
		ConnectivityManager cm = (ConnectivityManager) ctx.getSystemService(Context.CONNECTIVITY_SERVICE);
		return cm.getActiveNetworkInfo()!=null;
	}



    private String getUrlEncodeData(HashMap<String, String> params) {
        if(params == null){
            return "";
        }
        StringBuilder result = new StringBuilder();
        boolean first = true;
        for(Map.Entry<String, String> entry : params.entrySet()){
            try {
                if (first)
                    first = false;
                else
                    result.append("&");
                result.append(entry.getKey());
                result.append("=");
                result.append(entry.getValue());
                Utilities.log("server, " + entry.getKey() + " = " + entry.getValue());
            }
            catch(Exception e){e.printStackTrace();}
        }
        return result.toString();
    }

    public String httpGetData(String url){
        return httpGetData(url, null);
    }
    public String httpGetData(String url, HashMap<String, String> params){
        Utilities.log("ServerRequest - GET :: url = " + url + "?" + getUrlEncodeData(params));
        StringBuffer response = new StringBuffer();
        try {
            URLConnection conn = new URL(url).openConnection();
//            conn.setDoOutput(true);
            conn.setDoInput(true);
            conn.setRequestProperty("Content-Type","text/html; charset=UTF-8");


//            conn.setRequestProperty("X-APP-ID", Constants.APP_ID);
//            conn.setRequestProperty("X-AGENT-TOKEN", Constants.ANDROID_KEY);
//            conn.setRequestProperty("X-DEVICE-ID", Utilities.getDeviceId(context));
//            conn.setRequestProperty("X-APP-VERSION", Utilities.getVersionName(context));
            if(context != null) {
                conn.setRequestProperty("Authorization", new Pref(context).getPreferences(Pref.KEY_TOKEN));
                Log.d("authorization",new Pref(context).getPreferences(Pref.KEY_TOKEN));
            }

            OutputStreamWriter writer = new OutputStreamWriter(conn.getOutputStream());
            writer.write(getUrlEncodeData(params));
            writer.flush();
            writer.close();

            BufferedReader reader = new BufferedReader(new
                    InputStreamReader(conn.getInputStream()));
            String inputLine;

            while ((inputLine = reader.readLine()) != null) {
                response.append(inputLine);
            }
            reader.close();
        }
        catch(Exception e){e.printStackTrace();}
        Utilities.log("ServerRequest - GET :: url : " + url + getUrlEncodeData(params) + " Response : " + response.toString());
        return response.toString();
    }


    public String httpPostData(String url, HashMap<String, String> params){

        Utilities.log("ServerRequest - POST :: url = " + url + getUrlEncodeData(params));
        StringBuffer response = new StringBuffer();
        try {
            URL u = new URL(url);
            HttpURLConnection conn = (HttpURLConnection)u.openConnection();
            conn.setRequestMethod("POST");
            conn.setDoOutput(true);
            conn.setDoInput(true);
//            conn.setRequestProperty("X-APP-ID", Constants.APP_ID);
//            conn.setRequestProperty("X-AGENT-TOKEN", Constants.ANDROID_KEY);
//            conn.setRequestProperty("X-DEVICE-ID", Utilities.getDeviceId(context));
//            conn.setRequestProperty("X-APP-VERSION", Utilities.getVersionName(context));
//
//            Utilities.log("serverrequest, device id = " + Utilities.getDeviceId(context) + " , app version = " + Utilities.getVersionName(context));
//            if(context != null) {
//                conn.setRequestProperty("X-ACCESS-TOKEN", new Pref(context).getPreferences("u_token"));
//            }

            OutputStreamWriter writer = new OutputStreamWriter(conn.getOutputStream());

            writer.write(getUrlEncodeData(params));
            writer.flush();
            writer.close();

            BufferedReader reader = new BufferedReader(new
                    InputStreamReader(conn.getInputStream()));
            String inputLine;

            while ((inputLine = reader.readLine()) != null) {
                response.append(inputLine);
            }
            reader.close();
        }
        catch(Exception e){e.printStackTrace();}
        Utilities.log("ServerRequest - POST :: url : " + url + " Params : " + getUrlEncodeData(params) + " Response = " + response.toString());
        return response.toString();
    }

    public static HashMap<String, String> getSha1Hex(HashMap<String, String> params){
        String key = Constants.APP_ID + "_" + Constants.ANDROID_KEY + params.get("action");
        params.put("signature", getSha1Hex(key));
        Utilities.log(key);
        return params;
    }

    private static String getSha1Hex(String string){
        try{
            MessageDigest messageDigest = MessageDigest.getInstance("SHA-1");
            messageDigest.update(string.getBytes());
            byte[] bytes = messageDigest.digest();
            StringBuilder buffer = new StringBuilder();
            for (byte b : bytes){
                buffer.append(Integer.toString((b & 0xff) + 0x100, 16).substring(1));
            }
            Utilities.log("SHA-1 :: params = " + string + " signature = " + buffer.toString());
            return buffer.toString();
        }
        catch (Exception ignored){
            ignored.printStackTrace();
        }
        return "";
    }


}
