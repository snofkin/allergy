package com.example.amir.healthhome2.utils;

import android.app.Activity;
import android.app.ProgressDialog;

import com.example.amir.healthhome2.utilities.Utilities;


public class LoadingDialog {

	ProgressDialog dialog;
	Activity activity;

	public LoadingDialog(Activity activity) {
		this.activity = activity;
		this.dialog = new ProgressDialog(activity);
	}
	
	public void showDialog(){
            this.dialog.setMessage("Loading..");
            showD();
	}

    private void showD(){
        try {
            if(Utilities.isNetworkAvialable(activity)) {
                this.dialog.show();
            }
        }catch(Exception e){e.printStackTrace();}
    }

	public void showDialog(String message){
		this.dialog.setMessage(message);
        showD();
	}

	public void showDialog(String title, String message){
		this.dialog.setTitle(title);
		this.dialog.setMessage(message);
		showD();
	}
	public void showDialog(String title, String message, int icon){
		this.dialog.setIcon(icon);
		this.dialog.setTitle(title);
		this.dialog.setMessage(message);
		showD();
	}
	
	public void hideDialog(){
        try {
            this.dialog.cancel();
        }catch(Exception e){e.printStackTrace();}
	}
}
