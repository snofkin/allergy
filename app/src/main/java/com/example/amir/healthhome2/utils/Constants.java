package com.example.amir.healthhome2.utils;

/**
 * Created by subrat on 9/24/15.
 */
public class Constants {

    public static final String APP_ID = "";
    public static final String ANDROID_KEY = "";
    public static final String ANALYTICS_ID = "";
    public static final String PARSE_APP_ID = "";
    public static final String PARSE_CLIENT_KEY = "";
    public static final String GOOGLE_DEVELOPER_KEY = "";
    public static final String DISCLAIMER = "";
   public static String BASE_URL = "http://52.40.66.231/api/";
  // public static String BASE_URL = "http://192.168.0.106:8000/api/";
    //public static String BASE_URL = "http://hah.subratgyawali.com.np/api/";
//    public static String BASE_URL = "http://52.33.104.179/api/";
    public static String URL_APPOINTMENTS = BASE_URL+"appointment";
    public static String URL_ALL_APPOINTMENTS = BASE_URL+"allappointment/";
    public static String URL_FIX_APPOINTMENT = BASE_URL+"fixappointment";
}

