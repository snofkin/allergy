package com.example.amir.healthhome2.utilities;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager.NameNotFoundException;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.support.design.BuildConfig;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;

import com.example.amir.healthhome2.http.ServerRequest;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;


public class Utilities {

	public static final String BASE_URL = "http://api.pathshalaapp.com/v1/";
    public static final String SD_PATH = Environment.getExternalStorageDirectory() + "/PathshalaApp";
    public static final String[] DAYS = {"Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"};
    public static final String[] MONTHS = {"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};

    public static void log(String string){
        if(BuildConfig.DEBUG)
            Log.i("Utility", string);
    }
    public static String getPref(Context ctx, String key){
        SharedPreferences pf = PreferenceManager.getDefaultSharedPreferences(ctx);
        return pf.getString(key, "");
    }

    public static boolean setPref(Context ctx, String key, String val){
        SharedPreferences pf = PreferenceManager.getDefaultSharedPreferences(ctx);
        return pf.edit().putString(key, val).commit();
    }


    /*public static void sendEvent(String category, String action){
        Tracker t = MainApplication.tracker;
        if(t == null)return;
        t.send(new HitBuilders.EventBuilder()
                .setCategory(category)
                .setAction(action)
                .build());

    }
    public static void sendScreen(String screenName){
        Tracker t = MainApplication.tracker;
        if(t == null)return;
        t.setScreenName(screenName);
        t.send(new HitBuilders.EventBuilder()
                .build());

    }*/

    private static String getFileNameFromUrl(String url) {
        try {
            return url.substring(url.lastIndexOf("/") + 1);
        } catch (Exception e) {
        }
        return "";
    }

    public static File getSDPathFromUrl(String url) {
        return createFilePath(SD_PATH, getFileNameFromUrl(url));
    }

    public static boolean checkFileExist(String url) {
        if(url == null || url.isEmpty())
            return false;
        File f = createFilePath(SD_PATH, getFileNameFromUrl(url));
        return f.exists();
    }

    private static File createFilePath(String folder, String filename) {
        File f = new File(folder);
        if (!f.exists())
            f.mkdirs();
        return new File(f, filename);
    }


    public static boolean isLoggedIn(Context context) {
        SharedPreferences pf = PreferenceManager.getDefaultSharedPreferences(context);
        return !pf.getString("u_token", "").isEmpty();
    }
    public static Bitmap getCircleBitmap(Bitmap bitmap) {
        int smallest = bitmap.getWidth() < bitmap.getHeight() ? bitmap.getWidth() : bitmap.getHeight();
        Bitmap output = Bitmap.createBitmap(smallest, smallest, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(output);

        final int color = 0xffffffff;
        final Paint paint = new Paint();
        final Rect rect = new Rect(0, 0, smallest, smallest);
        final RectF rectF = new RectF(rect);

        paint.setAntiAlias(true);
        paint.setDither(true);
        paint.setFilterBitmap(true);
        canvas.drawARGB(0, 0, 0, 0);
        paint.setColor(color);
        canvas.drawOval(rectF, paint);
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(bitmap, rect, rect, paint);

        return output;
    }


    public static void doRate(Context context) {
		try {
			context.startActivity(new Intent(Intent.ACTION_VIEW, Uri
					.parse("market://details?id=" + context.getPackageName()))
					.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
		} catch (android.content.ActivityNotFoundException anfe) {
			context.startActivity(new Intent(Intent.ACTION_VIEW, Uri
					.parse("http://play.google.com/store/apps/details?id="
							+ context.getPackageName()))
					.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
		}

	}

	public static Intent getDefaultShareIntent(String name, String content) {
		Intent intent = new Intent(Intent.ACTION_SEND);
		intent.setType("text/plain");
		intent.putExtra(Intent.EXTRA_SUBJECT, name);
		intent.putExtra(Intent.EXTRA_TEXT, content);
		return intent;

	}

	public static boolean isNetworkAvialable(Context cont) {
		ConnectivityManager cm = (ConnectivityManager) cont
				.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo ni = cm.getActiveNetworkInfo();
		if (ni != null)
			return true;
		else
			return false;
	}



	public static String getVersionName(Context context){
		try {
			return context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionName;
		} catch (NameNotFoundException e) {
			e.printStackTrace();
		}
		return "";
	}
	public static int getVersioncode(Context context){
		int version = 1;
		try {
			version = context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionCode;
		} catch (NameNotFoundException e) {
			e.printStackTrace();
		}
		return version;
	}

    public static void shareApp(Context context) {
        context.startActivity(Intent.createChooser(Utilities
                .getDefaultShareIntent("college",
						"http://play.google.com/store/apps/details?id="
								+ context.getPackageName()), "Share this App").addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
    }


//    public static String getEmail(Context context) {
//        try {
//            return AccountManager.get(context).getAccountsByType("com.google")[0].name;
//        }catch(Exception e){}
//        return "";
//    }

    public static String getDeviceId(Context context){
        try {
            return Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
        }catch(Exception e){}
        return "0";
    }
    public static long getPrefNum(Context ctx, String key){
        SharedPreferences pf = PreferenceManager.getDefaultSharedPreferences(ctx);
        return pf.getLong(key, 0l);
    }

    public static boolean setPrefNum(Context ctx, String key, long val){
        SharedPreferences pf = PreferenceManager.getDefaultSharedPreferences(ctx);
        return pf.edit().putLong(key, val).commit();
    }

    public static boolean isCacheValid(Context ctx, String key){
        long last = getPrefNum(ctx, key);
        return (System.currentTimeMillis() - last < 60*60*1000 );
    }

    public static boolean isCacheValid(Context ctx, String key, long validity){
        long last = getPrefNum(ctx, key);
        return (System.currentTimeMillis() - last < validity );
    }

    public static String getDeviceInfo() {
        try {
            return Build.MANUFACTURER.toUpperCase() + " (" + Build.MODEL + ")";
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    public static void openLink(Context context, String url){
        if(url == null || url.isEmpty())
            return;
        try {
            context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(url)));
        }catch(Exception e){e.printStackTrace();}
    }

    public static void sendEmail(Context context, String name, String email) {
        Intent i = new Intent(Intent.ACTION_SEND);
        i.setType("message/rfc822");
        i.putExtra(Intent.EXTRA_EMAIL  , new String[]{email});
        i.putExtra(Intent.EXTRA_SUBJECT, name);
        i.putExtra(Intent.EXTRA_TEXT   , name);
        try {
            context.startActivity(Intent.createChooser(i, "Select action"));
        } catch (Exception e) {e.printStackTrace();}
    }

    public static String getTimeString(String time){
        Calendar cal = Calendar.getInstance();
        String d[] = time.split("-");
        if(d != null && d.length == 3) {
            cal.set(Integer.parseInt(d[0]), Integer.parseInt(d[1])-1, Integer.parseInt(d[2]));
        }
        return new SimpleDateFormat("MMM d, yyyy").format(cal.getTime());

    }
    public static void setListViewHeightBasedOnChildren(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null) {
            // pre-condition
            return;
        }

        int totalHeight = listView.getPaddingTop() + listView.getPaddingBottom();
        int desiredWidth = View.MeasureSpec.makeMeasureSpec(listView.getWidth(), View.MeasureSpec.AT_MOST);
        for (int i = 0; i < listAdapter.getCount(); i++) {
            View listItem = listAdapter.getView(i, null, listView);

            if(listItem != null){
                // This next line is needed before you call measure or else you won't get measured height at all. The listitem needs to be drawn first to know the height.
                listItem.setLayoutParams(new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT));
                listItem.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);
                totalHeight += listItem.getMeasuredHeight();

            }
        }

        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + (listView.getDividerHeight() * listAdapter.getCount());
        listView.setLayoutParams(params);
        listView.requestLayout();
    }

    public static void showAppDisabledDialog(final Activity activity) {
        android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(activity);
        builder.setTitle("App is inactive")
                .setMessage("It seems that this app has been disabled by Admin.\n\nPlease contact college for the information.")
                .setPositiveButton("OK", null)
                .setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialogInterface) {
                        try{
                            activity.finish();
                        }catch (Exception e){e.printStackTrace();};
                    }
                })
                .create().show();
    }

    public static boolean isValidEmail(String emailId) {
        if(emailId == null || emailId.isEmpty())
        return false;
        return emailId.matches("\\w[-.\\+_\\w]*@\\w[-._\\w]*\\w\\.\\w\\w+");
    }

    public static String replaceImageIfNoInternet(Context context, String html){
        if(!ServerRequest.isNetworkConnected(context)){
            html = html.replaceAll("\\<img.*?>", "<img src='file:///android_asset/no_image.jpg'/>");
        }
        return html;
    }


}
